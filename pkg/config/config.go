package dconfig

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// LoadConfig  takes a path and inits its contents
func LoadConfig(path string, cfg interface{}) error {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	return yaml.Unmarshal(b, &cfg)
}
